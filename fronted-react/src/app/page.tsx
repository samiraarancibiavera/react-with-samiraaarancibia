import Image from "next/image";
import styles from "./page.module.css";
import Button from "./components/Button";
import Navbar from "./components/Navbar";
import "@fontsource/inter";
import { Header } from "./components/Header";
import ButtonC from "./components/ButtonC";



export default function Home() {
  return (
    <main className="x"> 
      <section className="Head">
        <div className="nav">
        <Navbar></Navbar>
        </div>
      </section>
      <section className="Badge">
        <div>
          <div className="title">
            
            <h1>Breath Natureal</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <div className="tbtn">
              <Button title="Explore"></Button>
              <ButtonC title=""></ButtonC>
            </div>
          </div>
          <div>
          </div>  
        </div>      
        
      </section>
    </main>
  )
}