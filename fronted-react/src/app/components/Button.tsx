export default function Button(props: {title:string}){
    return(
        <div>
            <button className="btn btn-prin">{props.title}</button>
        </div>
         
    )
}