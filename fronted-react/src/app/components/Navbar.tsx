import Link from "next/link";

export default function Navbar(){
    return(
        <nav className="Nav-bar">
        <div className="logo">
            <img src="./images/logo1.png" alt="" />
            <h3>PLANTO</h3>
            
        </div>
        <div className="Nav-Links">
            <ul>
                <li>
                    <Link href="/">Home</Link>
                </li>
                <li>
                    <Link href="/">Plants Type</Link>
                </li>
                <li>
                    <Link href="/">More</Link>
                </li>
                <li>
                    <Link href="/">Contact</Link>
                </li>
            </ul>
        </div>
        <div className="icon-prin">
            <i><img src="/images/icon1.svg" alt="" /></i>
            <i><img src="/images/icon2.svg" alt="" /></i>
            <i><img src="/images/iicon3.svg" alt="" /></i>
        </div>
            
        
       </nav>

    )
}