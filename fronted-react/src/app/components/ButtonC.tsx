export default function ButtonC(props: {title:string}){
    return(
        <div>
           <button className="btn-circle">{props.title}</button>
        </div>
         
    )
}