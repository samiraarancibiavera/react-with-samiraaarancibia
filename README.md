# PROYECTO DE REACT AUX SIS313G1 - I/2024
**Created by:** Univ. Samira Arancibia Vera

## LINK DEL TEMPLATE EN FIGMA
https://www.figma.com/design/cQRjY9XJEm19KbFM0XBy2K/Interactive-Portfolio-Website%3A-Figma-UI-Design-Tutorial-for-Beginners-%7C-Step-by-Step-Guide-(Community)?node-id=0-1
## LINK DEL REPOSITORIO PRINCIPAL
https://gitlab.com/samiraarancibiavera/auxsis313g1-i24-samira-arancibia.git
## LINK NETLIFY
proximamente

## AVANCES DEL TEMPLATE
- [X] Header
- [ ] Skills Section
- [ ] Experience Section
- [ ] About Me Section
- [ ] Project Section
- [ ] Testimonial Section
- [ ] Contact Section
- [ ] Footer


